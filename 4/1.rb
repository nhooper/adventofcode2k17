#!/usr/bin/env ruby

require 'pp'

File.open('input.txt', 'r') do |f|
  passwords = []
  f.each_line do |l|
    passwords.push(l.split(/\s/).map { |x| x.chomp })
  end
  puts passwords.select { |password| password.uniq.length == password.length }.length
end
