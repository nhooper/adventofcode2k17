#!/usr/bin/env ruby

File.open('input.txt', 'r') do |f|
  rows = []
  f.each_line do |l|
    rows.push(l.split(/\t/).map { |x| x.chomp.to_i })
  end
  sum = rows.map do |row|
    [row.max, row.min]
  end.map do |pair|
    (pair[0] - pair[1]).abs
  end.inject(:+)
  puts sum
end
