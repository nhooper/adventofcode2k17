#!/usr/bin/env ruby

def member_at_index_matches_next_member(ary, i)
  length_div_2 = ary.length / 2
  ary[i] == ary[(i + length_div_2) % ary.length]
end

def sum_of_matching_adjecent_members(ary)
  matched = []
  for i in 0...ary.length
    matched.push(ary[i]) if member_at_index_matches_next_member(ary, i)
  end
  matched.map { |x| x.to_i }.inject(:+)
end

if $0 == __FILE__
  ary = nil
  File.open('input.txt', 'r') do |f|
    ary = f.read.split('')
    ary.pop
  end
  puts sum_of_matching_adjecent_members(ary)
end
