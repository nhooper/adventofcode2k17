#!/usr/bin/env ruby

def member_at_index_matches_next_member(ary, i)
  if i == (ary.length - 1)
    return ary[0] == ary[-1]
  else
    return ary[i] == ary[i + 1]
  end
end

def sum_of_matching_adjecent_members(ary)
  matched = []
  for i in 0...ary.length
    matched.push(ary[i]) if member_at_index_matches_next_member(ary, i)
  end
  matched.map { |x| x.to_i }.inject(:+)
end

if $0 == __FILE__
  ary = nil
  File.open('input.txt', 'r') do |f|
    ary = f.read.split('')
  end
  print sum_of_matching_adjecent_members(ary)
end
